import React from "react";

export const Header = () => {
  return (
    <div className="header">
      <div className="grid">
        <nav className="header-content">
          <ul className="header-content-list">
            <li className="header-content-item-account">
              <a href="">facebook</a>
              <button>Đăng ký</button>
            </li>
          </ul>
          <ul className="header-content-list">
            <li className="header-content-item-join">
              <a href="">Tham gia hoặc đăng nhập Facebook </a>
              <i className="header-content-icon fas fa-caret-down"></i>
              <div className="header-content-notify">
                <div className="header-content-notify-text">
                  <label>Email hoặc điện thoại</label>
                  <input />
                </div>

                <div className="header-content-notify-pass">
                  <label>Mật khẩu</label>
                  <input />
                </div>
                <div className="header-content-forgot">
                  <p>Quên tài khoản?</p>
                </div>
                <div className="header-content-btn">
                  <button>Đăng nhập</button>
                </div>
                <div className="header-content-text">
                  <p>Bạn có muốn tham gia Facebook không?</p>
                </div>
                <div className="header-content-btn-register">
                  <button>Đăng ký</button>
                </div>
              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default Header;
