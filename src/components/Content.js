import React from "react";

export const Content = () => {
  return (
    <div className="content-container">
      <div className="grid">
        <div className="grid-page">Tạo Trang</div>
        <div className="grid-connect">
          Hãy kết nối doanh nghiệp, bản thân hoặc mục đích xã hội của bạn với
          cộng đồng người dùng Facebook trên toàn thế giới. Để bắt đầu, hãy chọn
          hạng mục Trang.
        </div>
        <div className="content-create-page">
          <div className="content-create-company">
            <div className="content-create-company--facebook">
              <img
                src="https://www.facebook.com/images/pages/create/biz_illustration.png"
                alt=""
              />
            </div>
            <span>Doanh nghiệp hoặc Thương hiệu</span>
            <p className="content-create-company-introduce">
              Giới thiệu sản phẩm và dịch vụ của bạn, nêu bật thương hiệu và
              tiếp
            </p>
            <label className="content-create-company-customer">
              cận thêm khách hàng tiềm năng trên Facebook.
            </label>
            <button>Bắt đầu</button>
          </div>
          <div className="content-create-public">
            <div className="content-create-public--share">
              <img
                className="content-create-public-img"
                src="https://www.facebook.com/images/pages/create/community_illustration.png"
              />
            </div>
            <span>Cộng đồng hoặc Người của công chúng</span>
            <p className="content-create-company-connect--share">
              Kết nối và chia sẻ với mọi người trong cộng đồng, tổ chức, đội
              ngũ,
            </p>
            <label className="content-create-company-clup">
              nhóm hoặc câu lạc bộ của bạn.
            </label>
            <button>Bắt đầu</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Content;
